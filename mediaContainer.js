"use strict";

class MediaContainer {
	constructor(data) {
		data = data || {};
		
		this.mediaPlayer = null;
		this.notesParser = null;
		
		this.dom = data.dom || document.body;
		
		/* Media folder path */
		this.mediaBasePath = "";
		if(!/^(f|ht)tps?:\/\//i.test(data.mediaPath)) {
			if(data.mediaBasePath) {
				this.mediaBasePath = data.mediaBasePath;
			}
			else {
				this.mediaBasePath = data.mediaPath ? this.base(data.mediaPath) + "/" : "";
			}
		}
		this.mediaPath = data.mediaPath || "";
		this.mediaDataFileName = data.mediaDataFileName || "data.json";
		
		/* Media file type */
		this.mediaBasicType = data.mediaType ? this.base(data.mediaType) : "audio";
		this.mediaType = data.mediaType || "audio/mpeg";
		
		this.autoPlay = data.autoPlay || false;
		
		this.allowOverlayHiding = typeof data.allowOverlayHiding === typeof true ? data.allowOverlayHiding : true;
		
		/* Define how much should a playback be moved left/right using keyboard arrows.
		 * If not set defaults to a portion of the video duration
		 */
		this.videoArrowsSkippingStep = data.videoArrowsSkippingStep || null;
		
		/* Key offset */
		this.keyOffset = data.keyOffset && !isNaN(data.keyOffset) ? Number(data.keyOffset) : 0;
		
		/* Theme */
		this.playerTheme = data.playerTheme || null;
		
		this.createWrapper();
	}
	
	base(path) {
		return path.substr(0, path.lastIndexOf("/"));
	}
	
	updateContainerSize() {
		if(this.mediaPlayer) {
			this.mediaPlayer.style.removeProperty("width");
			this.mediaPlayer.style.removeProperty("height");			
			this.mediaPlayerContainer.style.removeProperty("width");
			this.mediaPlayerContainer.style.removeProperty("height");
			
			if(this.mediaPlayerWrapper) {
				let playerWidth = this.mediaPlayer.offsetWidth;
				let playerHeight = this.mediaPlayer.offsetHeight;
				let containerWidth = this.mediaPlayerWrapper.offsetWidth;
				let continerHeight = this.mediaPlayerWrapper.offsetHeight;
				
				this.mediaPlayerContainer.style.width = playerWidth > containerWidth ? containerWidth + "px" : playerWidth + "px";
				this.mediaPlayerContainer.style.height = playerHeight > continerHeight ? continerHeight + "px" : playerHeight + "px";
			}
			else {
				this.mediaPlayerContainer.style.width = this.mediaPlayer.offsetWidth + "px";
				this.mediaPlayerContainer.style.height = this.mediaPlayer.offsetHeight + "px";
			}
			
			let videoWidth = this.mediaPlayer.videoWidth;
			let videoHeight = this.mediaPlayer.videoHeight;
			let ratio = videoWidth/videoHeight;
			
			// curentWidth/currentHeight = videoWidth/videoHeight
			
			let newHeight = this.mediaPlayer.offsetWidth / ratio;
			let newWidth = this.mediaPlayer.offsetHeight * ratio;
			if(newHeight > this.mediaPlayerContainer.offsetHeight) {
				newHeight = this.mediaPlayerContainer.offsetHeight
			}
			else {
				newWidth = newHeight * ratio;
			}
			
			this.mediaPlayer.style.width = newWidth + "px";
			this.mediaPlayer.style.height = newHeight + "px";
			if(this.overlayContainer) {
				this.overlayContainer.style.width = newWidth + "px";
				this.overlayContainer.style.height = newHeight + "px";
			}
		}
	}
	
	loadMedia() {
		this.mediaPlayerWrapper = document.createElement("div");
		this.mediaPlayerWrapper.classList.add("mediaplayer-wrapper");
		this.columnLeft.appendChild(this.mediaPlayerWrapper);
		
		this.mediaPlayerContainer = document.createElement("div");
		this.mediaPlayerContainer.classList.add("mediaplayer-container");
		this.mediaPlayerWrapper.appendChild(this.mediaPlayerContainer);
		
		if(this.mediaBasicType === "audio") {
			this.createAudioPlayer();
		}
		else {
			this.createVideoPlayer();
		}
		
		this.controls = this.customControls(this.mediaPlayer);
		if(this.mediaBasicType === "video") {
			this.mediaPlayerWrapper.appendChild(this.controls);
		}
		else {
			this.mediaPlayerWrapper.insertBefore(this.controls, this.mediaPlayerContainer);
		}
		
		if(this.mediaBasicType === "video" && this.mediaPlayer) {
			this.updateContainerSize();
			this.mediaPlayer.addEventListener("loadeddata", () => {
				this.updateContainerSize();
			});
			this.mediaPlayer.addEventListener("play", () => {
				this.updateContainerSize();
			});
			window.addEventListener("resize", () => {
				this.updateContainerSize();
			});
		}
		
		/*
		document.addEventListener("fullscreenchange", (event) => {
			if(!this.mediaPlayerWrapper.classList.contains("fullscreen")) {
				this.togglePlayerFullscreen(true);
			}
		});
		document.addEventListener("mozfullscreenchange", (event) => {
			if(!this.mediaPlayerWrapper.classList.contains("fullscreen")) {
				this.togglePlayerFullscreen(true);
			}
		});
		document.addEventListener("webkitfullscreenchange", (event) => {
			if(!this.mediaPlayerWrapper.classList.contains("fullscreen")) {
				this.togglePlayerFullscreen(true);
			}
		});
		document.addEventListener("MSFullscreenChange", (event) => {
			if(!this.mediaPlayerWrapper.classList.contains("fullscreen")) {
				this.togglePlayerFullscreen(true);
			}
		});
		*/
	}
	
	createWrapper() {
		this.wrapper = document.createElement("div");
		this.wrapper.classList.add("media-container");
		if(this.playerTheme) {
			this.wrapper.classList.add(this.playerTheme);
		}
		this.dom.appendChild(this.wrapper);
		
		let columns = document.createElement("div");
		columns.classList.add("columns");
		this.wrapper.appendChild(columns);	
		
		let columnLeftContainer = document.createElement("div");
		columnLeftContainer.classList.add("column", "column-left");		
		columns.appendChild(columnLeftContainer);
		
		this.columnLeft = document.createElement("div");
		this.columnLeft.classList.add("inner-box");		
		columnLeftContainer.appendChild(this.columnLeft);
		
		let columnRightContainer = document.createElement("div");
		columnRightContainer.classList.add("column", "column-right");		
		columns.appendChild(columnRightContainer);
		
		this.columnRight = document.createElement("div");
		this.columnRight.classList.add("inner-box");		
		columnRightContainer.appendChild(this.columnRight);
	}
	
	populateMetadata() {
		let metadataContainer = document.createElement("div");
		metadataContainer.classList.add("metadata-container");
		
		let title = document.createElement("h3");
		title.classList.add("title");
		title.innerText = this.notesParser.getTitle();
		metadataContainer.appendChild(title);
		
		let description = document.createElement("p");
		description.classList.add("description");
		description.innerText = this.notesParser.getDescription();
		metadataContainer.appendChild(description);
		
		this.wrapper.insertBefore(metadataContainer, this.wrapper.firstChild);
	}
	
	createAudioPlayer() {
		this.mediaPlayer = document.createElement("audio");
		this.mediaPlayer.src = this.mediaPath;
		this.mediaPlayer.type = this.mediaType;
		this.mediaPlayer.autoplay = this.autoPlay;
		this.mediaPlayer.classList.add("player", "audio-player");
		this.wrapper.classList.add("audio");
		this.mediaPlayerContainer.appendChild(this.mediaPlayer);
	}
	
	createVideoPlayer() {
		this.mediaPlayer = document.createElement("video");
		this.mediaPlayer.src = this.mediaPath;
		this.mediaPlayer.type = this.mediaType;
		this.mediaPlayer.autoplay = this.autoPlay;
		this.mediaPlayer.classList.add("player", "video-player");
		this.wrapper.classList.add("video");
		this.mediaPlayerContainer.appendChild(this.mediaPlayer);
	}
	
	togglePlayerFullscreen(forceExit) {
		forceExit = forceExit || false;
		
		if(!forceExit) {
			this.mediaPlayerWrapper.classList.toggle("fullscreen");
		}
		else {
			this.mediaPlayerWrapper.classList.remove("fullscreen");
		}
		
		/*
		let enableFullscreen = (element) => {
			if(element.requestFullscreen) {
				element.requestFullscreen();
			} else if(element.mozRequestFullScreen) {
				element.mozRequestFullScreen();
			} else if(element.webkitRequestFullscreen) {
				element.webkitRequestFullscreen();
			} else if(element.msRequestFullscreen) {
				element.msRequestFullscreen();
			}
		}
		
		let exitFullscreen = () => {
			if(document.exitFullscreen) {
				document.exitFullscreen();
			} else if(document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if(document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
			}
		}
		*/
		
		if(this.mediaPlayerWrapper.classList.contains("fullscreen")) {
			document.body.style.height = "100%";
			document.body.style.overflow = "hidden";
			//enableFullscreen(this.mediaPlayerWrapper);
		}
		else {
			document.body.style.removeProperty("height");
			document.body.style.removeProperty("overflow");
			//exitFullscreen();
		}
		
		this.updateContainerSize();
	}
	
	customControls(mediaPlayer) {
		let controls = document.createElement("div");
		controls.classList.add("media-controls");
		mediaPlayer.addEventListener("play", () => {
			controls.classList.add("playing");
			controls.classList.remove("stopped");
			controls.classList.remove("ended");
		});
		
		mediaPlayer.addEventListener("pause", () => {
			controls.classList.remove("playing");
			controls.classList.add("stopped");
		});
		
		mediaPlayer.addEventListener("volumechange", () => {
			if(mediaPlayer.muted === true) {
				controls.classList.add("muted");
			}
			else {
				controls.classList.remove("muted");
			}
		});
		
		mediaPlayer.addEventListener("ended", () => {
			controls.classList.add("ended");
		});
		
		let buttonPlayPause = document.createElement("div");
		buttonPlayPause.innerText = "play/pause";
		buttonPlayPause.title = buttonPlayPause.innerText;
		buttonPlayPause.classList.add("button", "play-pause");
		buttonPlayPause.addEventListener("click", (event) => {
			if(mediaPlayer.paused || mediaPlayer.ended) {
				mediaPlayer.play();
			}
			else {
				mediaPlayer.pause();	
			}
			mediaPlayer.focus();
		});
		controls.appendChild(buttonPlayPause);
		
		let buttonStop = document.createElement("div");
		buttonStop.innerText = "stop";
		buttonStop.title = buttonStop.innerText;
		buttonStop.classList.add("button", "stop");
		buttonStop.addEventListener("click", (event) => {
			mediaPlayer.pause();
			mediaPlayer.currentTime = 0;
			mediaPlayer.focus();
		});
		controls.appendChild(buttonStop);
		
		/* Media progress */
		let mediaProgressPercentage = document.createElement("span");
		mediaProgressPercentage.classList.add("progress-time");
		mediaProgressPercentage.innerText = this.readableTime(mediaPlayer.currentTime);
		controls.appendChild(mediaProgressPercentage);
		
		let mediaProgress = document.createElement("div");
		mediaProgress.classList.add("progress-bar");
		let mediaProgressLineIndicator = document.createElement("div");
		mediaProgressLineIndicator.classList.add("progress-bar-line", "length-indicator");
		mediaProgress.appendChild(mediaProgressLineIndicator);
		let mediaProgressLine = document.createElement("div");
		mediaProgressLine.classList.add("progress-bar-line", "clickable");
		mediaProgressLine.style.width = "0%";
		mediaProgress.appendChild(mediaProgressLine);
		
		let getProgressTimeIndex = (pageX) => {
			let boundaries = mediaProgress.getBoundingClientRect();
			let relativePos = pageX - boundaries.left;
			if(relativePos >= 0 && relativePos <= boundaries.width) {
				let percentage = Math.floor((relativePos / boundaries.width) * 100);
				return (percentage * mediaPlayer.duration) / 100;
			}
			return null;
		}
		
		mediaProgress.addEventListener("click", (event) => {
			let timeIndex = getProgressTimeIndex(event.pageX);
			if(timeIndex !== null) {
				mediaPlayer.currentTime = timeIndex;
			}
			mediaPlayer.focus();
		});
		mediaProgress.addEventListener("mouseover", (event) => {
			let timeIndex = getProgressTimeIndex(event.pageX);
			if(timeIndex !== null) {
				mediaProgress.title = this.readableTime(timeIndex);
			}
		});
		this.mediaTimeIndexChange(() => {
			let percentage = Math.floor((100 / mediaPlayer.duration) * mediaPlayer.currentTime);
			if(percentage >= 0 && percentage <= 100) {
				mediaProgressLine.style.width = percentage + "%";
			}
			mediaProgressPercentage.innerText = this.readableTime(mediaPlayer.currentTime) + " / " + this.readableTime(mediaPlayer.duration);
		});
		controls.appendChild(mediaProgress);
		
		/* Sound */		
		let soundMenu = document.createElement("div");
		soundMenu.classList.add("submenu", "soundmenu");
		
		let soundMenuProgressWrapper = document.createElement("div");
		soundMenuProgressWrapper.classList.add("soundmenu-wrapper");
		
		let soundLevelClickable = document.createElement("div");
		soundLevelClickable.classList.add("sound-level", "clickable");
		
		let getSoundLevel = (pageY) => {
			let boundaries = soundMenuProgressWrapper.getBoundingClientRect();
			let relativePos = pageY - boundaries.top - document.documentElement.scrollTop;				
			if(relativePos >= 0 && relativePos <= boundaries.height) {
				return 1 - Math.floor((relativePos / boundaries.height) * 100) / 100;
			}
			return null;
		}
		
		soundLevelClickable.style.height = (mediaPlayer.volume * 100) + "%";
		soundMenuProgressWrapper.addEventListener("click", (event) => {
			let soundLevel = getSoundLevel(event.pageY);
			mediaPlayer.volume = soundLevel;
			mediaPlayer.focus();
		});
		
		let soundLevelGhost = document.createElement("div");
		soundLevelGhost.classList.add("sound-level", "ghost");
		
		soundMenuProgressWrapper.appendChild(soundLevelGhost);
		soundMenuProgressWrapper.appendChild(soundLevelClickable);
		soundMenu.appendChild(soundMenuProgressWrapper);
		
		let soundButton = document.createElement("div");
		soundButton.innerText = "sound";
		soundButton.classList.add("button", "sound");
		soundButton.addEventListener("click", (event) => {
			if(event.target === soundButton) {
				soundButton.classList.toggle("menu-visible");
				mediaPlayer.focus();
			}
		});
		
		let soundButtonVolumeIndicator = document.createElement("span");
		
		let volumeChangedSoundMenuMod = () => {
			let volume = Number(mediaPlayer.volume);
			let volumePercentage = Math.floor(volume * 100);
			soundLevelClickable.style.height = volumePercentage + "%";
			soundButton.classList.remove("mute", "low", "medium", "high");
			if(volume <= 0 || mediaPlayer.muted) {
				soundButton.classList.add("mute");
			}
			else if(volume <= 0.33) {
				soundButton.classList.add("low");
			}
			else if(volume <= 0.66) {
				soundButton.classList.add("medium");
			}
			else {
				soundButton.classList.add("high");
			}
			
			soundButtonVolumeIndicator.innerText = (mediaPlayer.muted ? "muted" : volumePercentage + "%");
		};
		
		mediaPlayer.addEventListener("volumechange", () => {
			volumeChangedSoundMenuMod();
		});
		
		volumeChangedSoundMenuMod();
		
		let muteButton = document.createElement("div");
		muteButton.innerText = "mute";
		muteButton.title = muteButton.innerText;
		muteButton.classList.add("button", "mute");
		muteButton.addEventListener("click", (event) => {
			mediaPlayer.muted = !mediaPlayer.muted;
			mediaPlayer.focus();
		});
		soundMenu.appendChild(muteButton);
		
		soundButton.appendChild(soundButtonVolumeIndicator);
		soundButton.appendChild(soundMenu);
		controls.appendChild(soundButton);
		
		if(this.mediaBasicType === "video") {
			let buttonFullScreen = document.createElement("div");
			buttonFullScreen.innerText = "fullscreen";
			buttonFullScreen.title = buttonFullScreen.innerText;
			buttonFullScreen.classList.add("button", "fullscreen");
			buttonFullScreen.addEventListener("click", (event) => {
				this.togglePlayerFullscreen();
				mediaPlayer.focus();
			});
			controls.appendChild(buttonFullScreen);
		}
		
		this.mediaPlayer.addEventListener("click", () => {
			if(this.mediaPlayer.paused) {
				this.mediaPlayer.play();
			}
			else {
				this.mediaPlayer.pause();
			}
		});
		
		this.mediaPlayer.addEventListener("dblclick", () => {
			this.togglePlayerFullscreen();
		});
		
		this.mediaPlayer.addEventListener("keydown", (event) => {
			/* Space */
			if(event.keyCode === 0 || event.keyCode === 32) {
				event.preventDefault();
				if(mediaPlayer.paused || mediaPlayer.ended) {
					mediaPlayer.play();
				}
				else {
					mediaPlayer.pause();	
				}
			}
			/* Escape */
			if(event.keyCode === 27) {
				this.togglePlayerFullscreen(true);
			}
			/* Left */
			if(event.keyCode === 37) {
				event.preventDefault();
				mediaPlayer.currentTime -= this.videoArrowsSkippingStep || Math.ceil(mediaPlayer.duration/20);
			}
			/* Right */
			if(event.keyCode === 39) {
				event.preventDefault();
				mediaPlayer.currentTime += this.videoArrowsSkippingStep || Math.ceil(mediaPlayer.duration/20);
			}
			/* Bottom */
			if(event.keyCode === 40) {
				event.preventDefault();
				if(mediaPlayer.volume > 0) {
					if(mediaPlayer.volume - 0.1 < 0) {
						mediaPlayer.volume = 0;
					}
					else {
						mediaPlayer.volume -= 0.1;
					}
				}
			}
			/* Up */
			if(event.keyCode === 38) {
				event.preventDefault();
				if(mediaPlayer.volume < 1) {
					if(mediaPlayer.volume + 0.1 > 1) {
						mediaPlayer.volume = 1;
					}
					else {
						mediaPlayer.volume += 0.1;
					}
				}
			}
		});
		
		return controls;
	}
	
	readableTime(time) {
		let twoDigits = function(number) {
			number = Math.floor(number);
			if(number < 10) {
				return "0" + number;
			}
			return number;
		}
		
		if(time < 60) {
			return "00:" + twoDigits(time);
		}
		else {
			let min = Math.floor(time/60);
			let sec = time % 60;
			
			if(min < 60) {
				return twoDigits(min) + ":" + twoDigits(sec);
			}
			else {
				let hour = Math.floor(min/60);
				min = min % 60;
				return twoDigits(hour) + ":" + twoDigits(min) + ":" + twoDigits(sec);
			}
		}
		return time;
	}
	
	createNotesContainer() {
		this.notesContainer = document.createElement("ul");
		this.notesContainer.classList.add("notes");
		if(this.notesParser) {
			let notes = this.notesParser.getNotes();
			for(let i = 0; i < notes.length; i++) {
				let noteDom = document.createElement("li");
				noteDom.dataset.index = notes[i].index;
				noteDom.innerText = notes[i].content;
				noteDom.classList.add("note", "inactive");
				this.notesContainer.appendChild(noteDom);
				
				let noteTimeIndex = document.createElement("span");
				noteTimeIndex.classList.add("time-index");
				noteTimeIndex.innerText = this.readableTime(notes[i].index);
				noteDom.appendChild(noteTimeIndex);
				
				noteDom.addEventListener("click", (event) => {
					if(this.mediaPlayer) {
						this.mediaPlayer.currentTime = event.target.dataset.index;
						this.mediaPlayer.play();
					}
				});
				noteDom.style.cursor = "pointer";
				
				this.setNoteBehaviour(notes, i, noteDom);
			}
		}
		this.columnRight.appendChild(this.notesContainer);
	}
	
	setNoteBehaviour(notes, index, dom) {
		this.mediaTimeIndexChange(() => {
			let timeIndex = this.mediaPlayer.currentTime;
			timeIndex -= this.keyOffset;
			
			let current = null;
			for(let i = 0; i < notes.length; i++) {
				if(notes[i].index <= timeIndex) {
					current = i;
				}
				else {
					break;
				}
			}
			
			if(current === index) {
				dom.classList.add("current");
			}
			else {
				dom.classList.remove("current");
			}
					
			if(timeIndex >= notes[index].index) {
				dom.classList.add("active");
				dom.classList.remove("inactive");
			}
			else {
				dom.classList.remove("active");
				dom.classList.add("inactive");
			}
		});
	}
	
	createOverlayContainer() {
		this.overlayContainer = document.createElement("div");
		this.overlayContainer.classList.add("overlay-container");
		
		if(this.overlayContainer) {
			let overlays = this.notesParser.getOverlays();			
			for(let i = 0; i < overlays.length; i++) {
				let overlayDom = null;
				
				if(overlays[i].url) {
					overlayDom = document.createElement("a");
					overlayDom.classList.add("clickable");
					let urlMatches = overlays[i].url.match(/^blank::(.*?)$/);			
					overlayDom.href = urlMatches ? urlMatches[urlMatches.length - 1] : overlays[i].url;
					overlayDom.title = overlayDom.href;
					if(urlMatches) {
						overlayDom.target = "_blank";
					}
				}
				else {
					overlayDom = document.createElement("div");
				}
				overlayDom.classList.add("overlay", "hidden");
				
				if(overlays[i].className) {
					overlayDom.classList.add(overlays[i].className);
				}
				this.overlayContainer.appendChild(overlayDom);
				if(overlays[i].content) {
					overlayDom.innerText = overlays[i].content;
				}
				if(overlays[i].icon) {
					overlayDom.classList.add("icon", overlays[i].icon);
					
					if(overlays[i].rotation && !isNaN(overlays[i].rotation)) {
						let rotation = Number(overlays[i].rotation);
						overlayDom.style.transform = "rotate(" + rotation + "deg)";
					}
				}
				
				if(overlays[i].css) {
					overlayDom.style = overlays[i].css;
				}
				
				if(this.mediaBasicType === "video") {					
					if(typeof overlays[i].x !== typeof undefined && overlays[i].x > 0) {
						overlayDom.style.left = overlays[i].x + "%";
					}
					if(typeof overlays[i].y !== typeof undefined && overlays[i].y > 0) {
						overlayDom.style.top = overlays[i].y + "%";
					}
					if(typeof overlays[i].width !== typeof undefined) {
						overlayDom.style.width = overlays[i].width + "%";
					}
					if(typeof overlays[i].height !== typeof undefined) {
						overlayDom.style.height = overlays[i].height + "%";
					}
				}
				
				this.setOverlayBehaviour(overlays[i], overlayDom);
			}
		}
		
		if(this.allowOverlayHiding) {
			this.controls.appendChild(this.overlayToggle());
		}
		
		this.mediaPlayerContainer.appendChild(this.overlayContainer);
	}
	
	overlayToggle() {
		let toggle = document.createElement("div");
		toggle.classList.add("button", "toggle-options-visibility");
		toggle.innerHTML = "show/hide overlay";
		toggle.title = toggle.innerHTML;
		toggle.style.cursor = "pointer";
		toggle.addEventListener("click", () => {
			this.overlayContainer.classList.toggle("hidden");
			if(this.overlayContainer.classList.contains("hidden")) {
				toggle.classList.add("is-hidden");
			}
			else {
				toggle.classList.remove("is-hidden");
			}
		});
		return toggle;
	}
	
	setOverlayBehaviour(overlay, dom) {
		this.mediaTimeIndexChange(() => {
			let timeIndex = this.mediaPlayer.currentTime;
			timeIndex -= this.keyOffset;
			
			if(typeof overlay.indexStart === typeof undefined && typeof overlay.indexEnd === typeof undefined) {
				dom.classList.add("active");
				dom.classList.remove("hidden");
			}
			else {
				if(typeof overlay.indexStart === typeof undefined) {
					if(timeIndex <= overlay.indexEnd) {
						dom.classList.add("active");
						dom.classList.remove("hidden");
					}
					else {
						dom.classList.remove("active");
						dom.classList.add("hidden");
					}
				}
				else if(typeof overlay.indexEnd === typeof undefined) {
					if(timeIndex >= overlay.indexStart) {
						dom.classList.add("active");
						dom.classList.remove("hidden");
					}
					else {
						dom.classList.remove("active");
						dom.classList.add("hidden");
					}
				}
				else {
					if(overlay.indexStart <= timeIndex && overlay.indexEnd >= timeIndex) {
						dom.classList.add("active");
						dom.classList.remove("hidden");
					}
					else {
						dom.classList.remove("active");
						dom.classList.add("hidden");
					}
				}
			}
		});
	}
	
	loadNotes() {
		this.notesParser = new NotesParser();
		return new Promise((resolve, reject) => {
			try {
				resolve(this.notesParser.loadFile(/^\//i.test(this.mediaDataFileName) ? this.mediaDataFileName : this.mediaBasePath + this.mediaDataFileName));
			}
			catch(error) {
				reject(Error(error));
			}
		});
	}
	
	mediaTimeIndexChange(call) {
		if(this.mediaPlayer && typeof call == "function") {
			this.mediaPlayer.addEventListener("timeupdate", () => {
				call();
			});
		}
	}
}
