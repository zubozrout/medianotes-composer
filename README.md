MediaNotes Composer
=========================

<p align="center">
  <img src="http://zubozrout.cz/data/thumbnail.png" alt="MediaNotes Composer example" style="max-width: 100%"/>
</p>

## Synopsis

MediaNotes Composer is a library created using modern JavaScript using ES6, relying on HTML5 and CSS3 functionality.

The utility simplyfies the process of creating audio and video player on a page while allowing some additional features, including chapter overview and overlay notes and icons. It works with video and audio objects for which it requires a json file with optional configuration to fulfill its role.

## Usage

Before you start you must include the following three files to the page:

```
<script src="notesParser.js"></script>
<script src="mediaContainer.js"></script>
<link rel="stylesheet" type="text/css" href="mediaNotes.css">
```

And another two files for full default theming support (see in the section below).

### To initialize an audio player you then need the following code:

```
let mediaContainerAudio = new MediaContainer({
		mediaPath: "media/path/audio.mp3",
		mediaDataFileName: "audio-data.json",
		mediaType: "audio/mpeg",
		dom: document.getElementById("media-audio-tag"),
		allowOverlayHiding: false,
		playerTheme: "light"
});
mediaContainerAudio.loadMedia();
mediaContainerAudio.loadNotes().then(() => {
	mediaContainerAudio.populateMetadata();
	mediaContainerAudio.createNotesContainer();
	mediaContainerAudio.createOverlayContainer();
});
```

### You can use very similar code to initialize a video player:

```
let mediaContainerVideo = new MediaContainer({
		mediaPath: "media/path/video.mp4",
		mediaDataFileName: "video-data.json",
		mediaType: "video/mpeg",
		autoPlay: true,
		dom: document.getElementById("media-video-tag"),
		playerTheme: "dark"
});
mediaContainerVideo.loadMedia();
mediaContainerVideo.loadNotes().then(() => {
	mediaContainerVideo.populateMetadata();
	mediaContainerVideo.createNotesContainer();
	mediaContainerVideo.createOverlayContainer();
});
```

Other optional attributes are:
* "keyOffset": a number in seconds which tells the player to postpone the notes and overlays by a defined number of seconds. Negative numbers will make the notes and overlays appear sooner.
* "mediaBasePath": If defined it tells the player where to find media source and metadata JSON file. If not defined it defaults to the media source folder and if metadata JSON file path doesn't contain a slash "/" then the library will expect to find it in the media source directory.
* "mediaPath" may also be an external file.

## Themes

The player comes with two supported themes by default - the "light" and the "bright". These can be defined by initializing the player with the "playerTheme" option and the respective string name.
However, if any of the above themes is to be used on the page the respective css file must be included on the page:

```
<link rel="stylesheet" type="text/css" href="mediaNotes-theme-dark.css">
<link rel="stylesheet" type="text/css" href="mediaNotes-theme-light.css">
```

If none of the above is linked the player will default to its factory theme - which is currently the dark one but this may change in the future - either by updating the dark theme itself or the factory one.

This also allows for custom themes which can be created eg. by modifyng the already created themes. You only need to change each style .media-container.dark or .media-container.light to .media-container.custom-name and use that custom-name when setting playerTheme option in the player init config.

## Metadata

Each media should have a pre-configured JSON metadata file containing a media title, description and a list of chapters and overlays. It is all optional but it is the reason this player exists.

A media metadata file may look like this:

```
{
	"title": "Media title",
	"description": "Some media description.",
	"keys": [
		{ "type": "note", "index": 0, "content": "Chapter 1" },
		{ "type": "overlay", "indexEnd": 20, "x": 10, "y": 10, "width": 20, "height": 20, "content": "See this overlay?" },
		{ "type": "note", "index": 19, "content": "Chapter 2" },
		{ "type": "overlay", "indexStart": 20, "indexEnd": 30, "x": 50, "y": 80, "width": 50, "height": 20, "css": "color: #fff; background: #000; border: 1px solid #fff;", "content": "Some overlay label 1" },
		{ "type": "overlay", "indexStart": 25, "indexEnd": 35, "x": 50, "y": 50, "width": 20, "height": 20, "css": "color: #fff; background: transparent;", "content": "Some overlay label 2" },
		{ "type": "overlay", "indexStart": 30, "content": "Some overlay label 3" },
		{ "type": "note", "index": 36, "content": "Chapter 3" },
		{ "type": "overlay", "indexStart": 2, "indexEnd": 12, "x": 20, "y": 35, "width": 30, "height": 30, "icon": "arrow", "url": "blank::https://google.com" },
		{ "type": "overlay", "indexStart": 2, "indexEnd": 12, "x": 35, "y": 20, "width": 30, "height": 30, "icon": "arrow_black", "rotation": 90 },
		{ "type": "overlay", "indexStart": 2, "indexEnd": 12, "x": 50, "y": 35, "width": 30, "height": 30, "icon": "arrow_white", "rotation": 180 },
		{ "type": "overlay", "indexStart": 2, "indexEnd": 12, "x": 35, "y": 50, "width": 30, "height": 30, "icon": "arrow_red", "rotation": 270 },
		{ "type": "overlay", "indexStart": 2, "indexEnd": 12, "x": 70, "y": 70, "width": 30, "height": 30, "icon": "arrow_blue", "rotation": -135 }
	]
}
```

Submited keys don't have to be in any specific order.

### Notes
Notes can be used both for the audio and video players.

* "index": Defines when a certain chapter starts. The passed number is the number of seconds from the beginning of the media start.

### Overlays
Overlays can be used for both players as well but those containing the following attributes: "x", "y", "width", "height", "icon" and "rotation" are only supported for the video player. Audio player overlays display below the player as a simple text note.

* "indexStart": A number of second from the playback start defining when the overlay should appear. If no "indexEnd" is defined then it stays visible until the end.
* "indexEnd": A number of second from the playback start defining when the overlay should disappear. If no "indexStart" is defined then it appears in the begining of the playback and stays visible till the "indexEnd" value is lower than current playback time.
* "x": Padding from the left border of the video player in %. (Video player only)
* "y": Padding from the top border of the video player in %. (Video player only)
* "width": Width of the overlay bubble in %. (Video player only)
* "height": Height of the overlay bubble in %. (Video player only)
* "css": Custom css string applied for the respective overlay note.
* "icon": A predefined icon theme. A custom theme can be added to CSS by defining the following selector style: `.media-container.video .overlay-container .overlay.icon.custom-icon-name {}`. (Video player only)
* "rotation": Allows for overlay bubbles rotation (in degrees). (Video player only)
* "url": Takes an url which can then be accessed by clicking the overlay. Url must start with a protocol like http:// or https:// and can have a "blank::" prefix which sets it to a target="_blank" - opening a link in a new tab or window.

### Common attribute overview
* "type": either "note" (chapter) or "overlay" (label).
* "content": A text content displayed in a note (chapter) or inside the overlay bubble. HTML is not allowed and if passed it will display it as plaintext. To make overlays clickable please refer to the url attribute.

## License

This project is available under GPLv3 license.
