"use strict";

class NotesParser {
	constructor() {		
		this.fetched = false;
		this.noteData = {};
	}
	
	assigned() {
		return this.fetched;
	}
	
	parseNotes(data) {
		data = data || {};
		
		this.noteData.title = data.title || null;
		this.noteData.description = data.description || null;
		this.noteData.notes = data.keys ? this.extractType(data.keys, "note") : [];
		this.noteData.overlays = data.keys ? this.extractType(data.keys, "overlay") : [];
		
		this.fetched = true;
		
		return this.noteData;
	}
	
	getTitle() {
		if(this.assigned()) {
			return this.noteData.title;
		}
		return "";
	}
	
	getDescription() {
		if(this.assigned()) {
			return this.noteData.description;
		}
		return "";
	}
	
	extractType(keys, type) {
		keys = keys || [];
		let extracted = [];
		for(let i = 0; i < keys.length; i++) {
			if(keys[i].type === type) {
				extracted.push(keys[i]);
			}
		}
		return extracted;
	}
	
	getNotes() {
		if(this.assigned()) {
			return this.noteData.notes;
		}
		return [];
	}
	
	getOverlays() {
		if(this.assigned()) {
			return this.noteData.overlays;
		}
		return [];
	}
	
	getNote(time) {
		if(this.assigned()) {
			time = time + this.noteData.keyOffset;
			console.log(time);
			let notes = this.getNotes();
			for(let i = 0; i < notes.length; i++) {
				if(notes[i].index === time) {
					return notes[i];
				}
			}
		}
		return null;
	}
	
	getOverlay(time) {
		if(this.assigned()) {
			time = time + this.noteData.keyOffset;
			console.log(time);
			let overlays = this.getOverlays();
			for(let i = 0; i < overlays.length; i++) {
				let start = overlays[i].indexStart;
				let end = overlays[i].indexEnd;
				if(time >= start && time <= end) {
					return overlays[i];
				}
			}
		}
		return null;
	}
	
	loadFile(url) {
		if(url) {
			return new Promise((resolve, reject) => {
				this.getRemoteContent(url).then((response) => {
					try {
						resolve(this.parseNotes(JSON.parse(response)));
					}
					catch(error) {
						reject(Error("Error in parsing remote mediaNote content as JSON: " + url, error));
					}
				});
			});
		}
		return false;
	}

	getRemoteContent(url) {
		return new Promise((resolve, reject) => {
			let xhr = new XMLHttpRequest();
			xhr.onreadystatechange = () => {
				if(xhr.readyState == 4) {
					if(xhr.status == 200) {
						resolve(xhr.response);
					}
					else {
						reject(Error(xhr.statusText));
					}
				}
			};
			xhr.onerror = function() {
				reject(Error("Network Error"));
			};
			xhr.open("GET", url, true);
			xhr.send();
		});
	}
}
